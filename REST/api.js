
// The API requires Express to work. This enables us to use Express' features
var express = require('express'); 

// Uses the date sent by a user
var bodyParser = require('body-parser');

// Encodes the user's password
var bcryptNodejs = require("bcrypt-nodejs");

// Used to connect to the database
var mysql = require('mysql');

// Create the connection to the database
var con = mysql.createConnection({
	host: "10.64.128.93",
	user: "Jeremy",
	password: "",
	database: "a2_projet_2_test",
	multipleStatements: true
});

// Checks the connection
con.connect(function(err) {
	if (err) throw err;
	console.log("Connected!");
});



// Create the Express object 
var app = express(); 
// we do not want to encode the url and we want get the message in json	
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// We verify if the account had a token 
function verifyToken(req, res, required_perms, callback) {
	if (!req.body.token) {
		// No token -> 401 Unauthorized
		res.sendStatus(401);
		callback(req,res,false);
	}
	// Return information about an account using its token
	con.query("CALL getAccountByToken(?)", req.body.token, function(error, rows, fields){
		if (error){
			console.log(error);
			res.sendStatus(500);
		} else {
			//If the account doesn't exist
			if (rows[0].length <= 0) {
				// No account with the specified mail -> 404 Not Found
				res.sendStatus(401);
				callback(req,res,false);
				return;
			}
			// Gets the status of an account
			var perm_level = rows[0][0].status;
			// If the account doesn't have permission
			if (!required_perms.includes(perm_level)) {
				// Forbidden to  client -> 403 Forbidden
				res.sendStatus(403);
				callback(req,res,false);
				return;
				// Account has permission, get the Id
			} else {
				var id = rows[0][0].id_account;
				callback(req,res,id);
				return;
			}
		}

	})
	
}

//	*****************************GET************************
// Display account information
app.get('/account/id/:id_account', function(req,res){
	console.log(req.params);
	// The parameter used by procedure
	var post = [[
	req.params.id_account
	]];
	// Calls the database and inputs the SQL query with post had parameters; them returns the result of the query
	con.query("CALL getAccountById ?", [post], function(error, rows, fields) {
		if (error){
			//500 = internal server error
			res.sendStatus(500);
		} else {
			//Sends the data using JSON formate
			res.json(rows);
		}
	})

})



//Gets all ideas from a campus
app.get('/ideas/:id_campus', function(req,res){
	console.log(req.params);
	//If there is no campus, return all ideas from all campus
	if (req.params.id_campus == 0) {
		con.query("CALL getIdeas", function(error, rows, fields){
			if (error){
				console.log(error);
				res.sendStatus(500);
			} else {
				res.json(rows[0]);
			}
		});

		
	} else {
		//If param is different from 0, use post to enter the parameter in the SQL query
		var post = [[req.params.id_campus]];
		con.query("CALL getAllIdeasFromCampus ?", [post], function(error, rows, fields){
			if (error){
				console.log(error);
				res.sendStatus(500);
			} else {
				res.json(rows[0]);
			}
		}
		)}

	});


// Get all event from a campus
app.get('/events/:id_campus', function(req,res){
	console.log(req.params);
	if (req.params.id_campus == 0) {
		con.query("CALL getAllEvents ", function(error, rows, fields){
			if (error){
				console.log(error);
				res.sendStatus(500);
			} else {
				res.json(rows[0]);
			}
		});
	} else {
		var post = [[req.params.id_campus]];
		con.query("CALL getAllEventsFromCampus ?", [post], function(error, rows, fields){
			if (error){
				console.log(error);
				res.sendStatus(500);
			} else {
				res.json(rows[0]);
			}
		}
		)}

	});


// Gets the token of an account (when connecting to the website)
app.get('/token', function(req, res){
	console.log(req.body);
	var post = [[
	req.body.mail

	]];
	// Gets the hashed password from the database 
	con.query("CALL getPasswordByMail ?", [post], function(error, rows, fields) {
		console.log(rows);
		if (error) {
			console.log(error);
			res.sendStatus(500);
		} else {
			console.log(rows);

			// Checks if the account exists
			if (rows[0].length <= 0) {
				// No account associated with the mail input -> 404 Not Found
				res.sendStatus(404);
			} else {
				// Compares the password input with the hashed password from the database
				if (!bcryptNodejs.compareSync(req.body.password, rows[0][0].password)) res.sendStatus(404);
				else {
					// Generates a token
					var token = Math.random()+"tk";

					// Uptade the database with the new token
					var updateArgs = [[
					req.body.mail,
					token
					]];
					con.query("CALL updateToken ?", [updateArgs], function(error, rows, fields)
					{
						if (error) {
							console.log(error);
							res.sendStatus(500);
						} else {
						// Creation and sending of the JSON containing the new token
						var obj = { token: token };
						res.json(obj);	
					}
				}
				)

				}
			}
		}
	})
	
})


// Gets an event using its Id
app.get('/event/:id_event', function(req,res){
	console.log(req.params);
	var post = [[req.params.id_event]];
	//Gets the event
	con.query("CALL getEventById ?", [post], function(error, rows, fields){
		var event = rows[0][0];
		var participants;
		var pictures;
		var id_e = [[req.params.id_event]];
		
		// Gets the participants of an event
		con.query("CALL getAllRegistered ?", [id_e], function(error, rows, fields){
			participants = rows[0];
			var id_event = [[req.params.id_event]];

			// Gets the pictures concerning an event
			con.query("CALL getPicturesFromEvent ?", [id_event], function(error, rows, fields){
				pictures = rows[0];

				// Adds the participants and the pictures to the event object
				event.participants=participants;
				event.pictures=pictures;
				res.json(event);
			})
		})
	})
	
})	


// Gets the different categories
app.get('/categories', function(req,res){
	console.log(req.body);


	con.query("CALL getCategories ", function(error, rows, fields) {
		if (error){
			res.sendStatus(500);
		} else {
			res.json(rows[0]);
		}
	})

})




//getEventFull
/*app.get('/ag', function(req,res){
	console.log(req.body);
	var post = [[req.body.id_e]];

	con.query("CALL getEventFull ?", [post], function(error, rows, fields) {
		if (error){
			throw error;
		} else {
			res.json(rows);
		}
	})

})*/

//Récupérer le mdp par le mail
/*app.get('/ad', function(req,res){
	console.log(req.body);
	var post = [[req.body.mail]];

	con.query("CALL getPasswordByMail ?", [post], function(error, rows, fields) {
		if (error){
			throw error;
		} else {
			res.json(rows);
		}
	})

})*/

// Gets all existing products from the shop
app.get('/products', function(req,res){
	console.log(req.body);


	con.query("CALL getProducts ", function(error, rows, fields) {
		// Required for AJAX queries to not be blocked by browsers
		res.set("Access-Control-Allow-Origin", "*");
		if (error){
			res.sendStatus(500);
		} else {
			res.json(rows[0]);
		}
	})

})

// Filter by category 
app.get('/product/cat/:id_category', function(req,res){
	console.log(req.body);
	console.log(req.params);
	var post = [[req.params.id_category]];

	con.query("CALL filterByCategory ?", [post], function(error, rows, fields) {
		if (error){
			res.sendStatus(500);
		} else {
			res.json(rows[0]);
		}
	})

})

// Filter by price
app.get('/product/price/:order_type', function(req,res){
	console.log(req.params);
	var post = [[req.params.order_type]];

	con.query("CALL orderByPrice ?", [post], function(error, rows, fields) {
		if (error){
			res.sendStatus(500);
		} else {
			res.json(rows[0]);
		}
	})

})

//Gets account information using a token
app.get('/account/token/:token', function(req,res){
	console.log(req.parms);
	var post = [[req.params.token]];

	con.query("CALL getAccountByToken ?", [post], function(error, rows, fields) {
		if (error){
			// 404 -> Error not found
			res.sendStatus(404)
		} else {
			// Building the response to the query
			account = rows[0][0]
			var obj = { id: account.id_account,
				name: account.name,
				surname: account.surname,
				status: account.status,
				campusID: account.id_campus
			};
			// Sending the object
			res.json(obj);
		}
	})

})

// Display the notification of a user
app.get('/notifications/:id_account', function(req, res){
	console.log(req.params);
	// Verify if the account has permission to execute the query
	verifyToken(req, res, [1,2,3,4], function(req, res, id) {
		// If the account doesn't exist or doesn't have permission stop the query
		if (!id) return;
		var post = [[
		id
		]];
		con.query("CALL getNotificationsOfUser ?", [post], function(error, rows, fields ){
			if (error){
				res.sendStatus(500);
			} else {
				res.json(rows[0]);

			}
		})

	})
})

//	*****************************POST************************	
// Adds an account
app.post('/account', function(req,res) {
	console.log(req.body);
	var post = [[
	req.body.surname,
	req.body.name,
	req.body.email,
	// Hashes the password received from the website
	bcryptNodejs.hashSync(req.body.password),
	req.body.campusID

	]];


	con.query("CALL addAccount ?", [post], function(error, rows, fields){
		if (error){
			console.log(error);
			// If the account already exist
			if (error.errno==1062)
				res.sendStatus(409);
			// If a field is incorrect
			else res.sendStatus(400);	
		} else {
			res.sendStatus(201);
			
		}
	})
})
//Registers an account to an event
app.post('/event/:id_event/participants', function(req,res) {
	console.log(req.body);

	verifyToken(req, res, [1,2,4], function(req, res, id) {
		if (!id) return;
		var post = [[
		req.params.id_event,
		id
		]];
		con.query("CALL addAccountToEvent ?", [post], function(error, rows, fields){
			if (error){
				if (error.errno==1062)
					// Account already register to the event
				res.sendStatus(202);
				else 
					{console.log(error);
						res.sendStatus(404)}
					} else {
						res.sendStatus(201);

					}

				})
	});
})

//Adds an event
app.post('/event', function(req,res) {
	console.log(req.body);
	var post = [[
	req.body.name,
	req.body.desc,
	req.body.place,
	req.body.date,
	req.body.recurrency,
	req.body.price,
	req.body.campusIDs
	]];

	verifyToken(req, res, [2,4], function(req, res, id){

		if (!id) return;


		con.query("CALL addEvent ?", [post], function(error, rows, fields){
			if (error){		
				console.log(error);
				// Bad request
				res.sendStatus(400);
			} else {
				res.sendStatus(201);	
			}
		})
	})
})

// Adds an idea
app.post('/idea', function(req,res) {
	console.log(req.body);
	verifyToken(req, res, [2,4], function(req, res, id) {
		if (!id) return;

	var post = [[
	req.body.desc,
	id
	]];
		con.query("CALL addIdea ?", [post], function(error, rows, fields){
			if (error){
				res.sendStatus(400);
			} else {
				res.sendStatus(201);

			}
		})
	})
})

// Adds a product
app.post('/product', function(req,res) {
	console.log(req.body);
	var post = [[
	req.body.name,
	req.body.desc,
	req.body.price,
	req.body.stock,
	req.body.categoryID	

	]];

	verifyToken(req, res, [2,4], function(req, res, id) {
		if (!id) return;	

		con.query("CALL addProduct ?", [post], function(error, rows, fields){
			if (error){
				console.log(error);
				if (error.errno==1366)
					res.sendStatus(400);
				else res.sendStatus(404);
			} else {
				res.sendStatus(201);

			}
		})
	})
})

// Adds a vote to an idea
app.post('/idea/:id_idea/vote', function(req,res) {
	console.log(req.params);
	console.log(req.body);
	

	verifyToken(req, res, [1,2,3,4], function(req, res, id) {
		if (!id) return;

		var post = [[
		req.params.id_idea,
		id
		]];

		con.query("CALL addVoteToIdea ?", [post], function(error, rows, fields){
			res.set("Access-Control-Allow-Origin", "*");
			if (error){
				if (error.errno==1062)
					// Already voted
				res.sendStatus(202);
				else 
					{console.log(error);
						res.sendStatus(500)}
					} else {
						res.sendStatus(201);


					}
				})
	})
})

//Adds a comment to a picture
app.post('/comment', function(req,res) {
	console.log(req.body);
	

	verifyToken(req, res, [1,2,3,4], function(req, res, id) {
		if (!id) return;

		var post = [[
		req.body.content,
		req.body.authorID,
		req.body.imgID
		]];

		con.query("CALL addComment ?", [post], function(error, rows, fields){
			if (error){
				res.sendStatus(400);
			} else {
				res.sendStatus(201);

			}
		})
	})
})

// Adds a like to a picture
app.post('/picture/:id_img/vote', function(req,res) {
	console.log(req.params);
	console.log(req.body);
	

	verifyToken(req, res, [1,2,4], function(req, res, id) {
		if (!id) return;

		var post = [[
		id,
		req.params.id_img
		]];

		con.query("CALL addLikeToPicture ?", [post], function(error, rows, fields){
			if (error){
				if (error.errno==1062)
					res.sendStatus(202);
				else 
					{console.log(error);
						res.sendStatus(500)}
					} else {
						res.sendStatus(201);

					}
				})
	})
})

// Adds a picture to an event
app.post('/picture', function(req,res) {
	console.log(req.body);
	

	verifyToken(req, res, [1,2,4], function(req, res, id) {
		if (!id) return;

		var post = [[
		req.body.url,
		id,
		req.body.eventID
		]];

		con.query("CALL addPictureToPastEvent ?", [post], function(error, rows, fields){
			if (error){
				res.sendStatus(400);
			} else {
				res.sendStatus(201);

			}
		})
	})
})



// Adds a category
app.post('/category', function(req,res) {
	console.log(req.body);
	var post = [[
	req.body.name
	]];

	verifyToken(req, res, [2,4], function(req, res, id) {
		if (!id) return;


		con.query("CALL addCategory ?", [post], function(error, rows, fields){
			if (error){
				res.sendStatus(400);
			} else {
				res.sendStatus(201);

			}
		})
	})
})



// Adds a product to an order
app.post('/order/:id_product', function(req,res) {
	console.log(req.body);
	var post = [[
	req.params.id_product,
	req.body.id,
	req.body.quantity
	]];

	con.query("CALL addProductToOrder ?", [post], function(error, rows, fields){
		if (error){
			res.sendStatus(400);
		} else {
			res.sendStatus(201);
			
		}
	})
})


// Create an order
app.post('/order', function(req,res) {
	console.log(req.body);
	

	verifyToken(req, res, [1,2,3,4], function(req, res, id) {
		if (!id) return;

		var post = [[
		id
		]];

		con.query("CALL createOrder ?", [post], function(error, rows, fields){
			if (error){
				res.sendStatus(400);
			} else {
				res.sendStatus(201);

			}
		})
	})
})

// Promote an idea into an event
app.post('/idea/:id_idea/promote', function(req,res) {
	console.log(req.body);
	console.log(req.params);
	var post = [[
	req.params.id_idea,
	req.body.name,
	req.body.desc,
	req.body.place,
	req.body.date,
	req.body.recurrency,
	req.body.price
	]];

	verifyToken(req, res, [2,4], function(req, res, id) {
		if (!id) return;


		con.query("CALL promoteIdeaoIntoEvent ?", [post], function(error, rows, fields){
			if (error){
				console.log(error);
				if (error.errno==1366)
					res.sendStatus(400);
				else res.sendStatus(404);
			} else {
				res.sendStatus(201);

			}
		})
	})

})



//	*****************************DELETE************************
// Deletes an account
app.delete('/account/:id_account', function(req, res){
	console.log(req.params);
	var post = [[
	req.params.id_account	 
	]];

	verifyToken(req, res, [2,4], function(req, res, id) {
		if (!id) return;


		con.query('CALL deleteAccount ?', [post], function(error, rows, fields){
			if (error){
				res.sendStatus(404);
			} else {
				// 204 -> No content = delete succesfull
				res.sendStatus(204);

			}
		})
	})
})

// Deletes an idea
app.delete('/idea/:id_idea', function(req, res){
	console.log(req.params);
	var post = [[
	req.params.id_idea	 
	]];

	verifyToken(req, res, [2,4], function(req, res, id) {
		if (!id) return;


		con.query('CALL deleteIdea ?', [post], function(error, rows, fields){
			if (error){
				res.sendStatus(404);
			} else {
				res.sendStatus(204);

			}
		})
	})
})

// Deletes a product
app.delete('/product/:id_product', function(req, res){
	console.log(req.params);
	var post = [[
	req.params.id_product	 
	]];
	verifyToken(req, res, [2,4], function(req, res, id) {
		if (!id) return;

		con.query('CALL deleteProduct ?', [post], function(error, rows, fields){
			if (error){
				res.sendStatus(404);
			} else {
				res.sendStatus(204);

			}
		})
	})
})

// Remove oneself from an event
app.delete('/event/:id_event/participants', function(req, res){
	
	console.log(req.body);
	
	verifyToken(req, res, [1,2,4], function(req, res, id) {
		if (!id) return;



		var post = [[
		req.params.id_event,
		id 
		]];



		con.query('CALL removePersonFromEvent ?', [post], function(error, rows, fields){
			if (error ){
				res.sendStatus(404);
			} else {
				if (rows.affectedRows ==0)
					// Already removed or was never registered on this event
				res.sendStatus(202);
				else
					res.sendStatus(204);

			}
		})
	})

})

// Removes a vote from an idea
app.delete('/idea/:id_idea/vote', function(req, res){
	console.log(req.body);

	verifyToken(req, res, [1,2,3,4], function(req, res, id) {
		if (!id) return;


		var post = [[
		req.params.id_idea,
		id
		]];

		con.query('CALL removeVoteFromIdea ?', [post], function(error, rows, fields){
			if (error){
				res.sendStatus(404);
			} else {
				res.sendStatus(204);

			}
		})
	})
})

// Removes a comment
app.delete('/comment/:id_comment', function(req, res){
	console.log(req.body);
	var post = [[
	req.params.id_comment
	]];

	verifyToken(req, res, [2,4], function(req, res, id) {
		if (!id) return;


		con.query('CALL deleteComment ?', [post], function(error, rows, fields){
			if (error){
				res.sendStatus(404);
			} else {
				res.sendStatus(204);

			}
		})
	})
})


// Deletes an event
app.delete('/event/:id_event', function(req, res){
	console.log(req.params);
	var post = [[
	req.params.id_event
	]];

	verifyToken(req, res, [2,4], function(req, res, id) {
		if (!id) return;


		con.query('CALL deleteEvent ?', [post], function(error, rows, fields){
			if (error){
				res.sendStatus(404);
			} else {
				res.sendStatus(204);

			}
		})
	})
})

// Deletes a picture
app.delete('/picture/:id_img', function(req, res){
	console.log(req.params);
	var post = [[
	req.params.id_img
	]];
	verifyToken(req, res, [2,4], function(req, res, id) {
		if (!id) return;


		con.query('CALL deletePicture ?', [post], function(error, rows, fields){
			if (error){
				res.sendStatus(404);
			} else {
				res.sendStatus(204);

			}
		})
	})
})

// Removes a like from a picture
app.delete('/picture/:id_img/vote', function(req, res){
	console.log(req.params);
	console.log(req.body);

	verifyToken(req, res, [1,2,3,4], function(req, res, id) {
		if (!id) return;


		var post = [[
		req.params.id_img,
		id
		]];



		con.query('CALL removeLikeFromPicture ?', [post], function(error, rows, fields){
			if (error){
				res.sendStatus(404);
			} else {
				res.sendStatus(204);

			}
		})
	})
})


// Deletes a notification
app.delete('/notification/:id_notification', function(req, res){
	console.log(req.body);
	var post = [[
	req.params.id_notification
	]];

	verifyToken(req, res, [1,2,3,4], function(req, res, id) {
		if (!id) return;


		con.query('CALL deleteNotifacation ?', [post], function(error, rows, fields){
			if (error){
				res.sendStatus(404);
			} else {
				res.sendStatus(204);

			}
		})
	})
})

// Deletes a category
app.delete('/category/:id_category', function(req, res){
	console.log(req.body);
	var post = [[
	req.params.id_category
	]];

	verifyToken(req, res, [2,4], function(req, res, id) {
		if (!id) return;


		con.query('CALL deleteCategory ?', [post], function(error, rows, fields){
			if (error){
				res.sendStatus(404);
			} else {
				res.sendStatus(204);

			}
		})
	})
})

// Deletes an order
app.delete('/order/:id_order', function(req, res){
	console.log(req.body);
	var post = [[
	req.params.id_order
	]];
	verifyToken(req, res, [2,4], function(req, res, id) {
		if (!id) return;

		con.query('CALL deleteOrder ?', [post], function(error, rows, fields){
			if (error){
				res.sendStatus(404);
			} else {
				res.sendStatus(201);

			}
		})
	})
})
// Deletes a product from an order
app.delete('/order/:id_order/:id_product', function(req, res){
	console.log(req.body);
	var post = [[
	req.params.id_order,
	req.params.id_product
	]];

	verifyToken(req, res, [2,4], function(req, res, id) {
		if (!id) return;


		con.query('CALL removeProductFromOrder ?', [post], function(error, rows, fields){
			if (error){
				console.log(error);
				if (error.errno==1366)
					res.sendStatus(400);
				else res.sendStatus(404);
			} else {
				res.sendStatus(204);

			}
		})
	})
})
//	*****************************PATCH************************
// Modifies an account information
app.patch('/account/:id_account', function(req, res){
	console.log(req.params)
	console.log(req.body);

	var post = [[
	req.params.id_account,
	req.body.surname,
	req.body.name,
	req.body.email,
	bcryptNodejs.hashSync(req.body.password),,
	req.body.campusID
	]];

	verifyToken(req, res, [1,2,4], function(req, res, id) {
		if (!id) return;


		con.query('CALL updateAccount ?', [post], function(error, rows, fields){
			if (error){
				console.log(error);
				if (error.errno==1366)
					res.sendStatus(400);
				else res.sendStatus(404);
			} else {
				res.sendStatus(204);

			}
		})
	})
})


// Modifies an event
app.patch('/event/:id_event', function(req, res){
	console.log(req.params)
	console.log(req.body);

	var post = [[
	req.params.id_event,
	req.body.name,
	req.body.desc,
	req.body.place,
	req.body.date,
	req.body.recurrency,
	req.body.price
	]];

	verifyToken(req, res, [1,2,4], function(req, res, id) {
		if (!id) return;


		con.query('CALL uptdateEvent ?', [post], function(error, rows, fields){
			if (error){
				console.log(error);
				if (error.errno==1366)
					res.sendStatus(400);
				else res.sendStatus(404);
			} else {
				res.sendStatus(204);

			}
		})
	})
})


// Updates the category a product belongs to
app.patch('/product/:id_product/cat/:id_category', function(req, res){
	console.log(req.params)
	console.log(req.body);

	var post = [[
	req.params.id_product,
	req.params.id_category
	]];

	verifyToken(req, res, [2,4], function(req, res, id) {
		if (!id) return;

		con.query('CALL uptdateProductCategory ?', [post], function(error, rows, fields){
			if (error){
				console.log(error);
				if (error.errno==1366)
					res.sendStatus(400);
				else res.sendStatus(404);

			} else {
				res.sendStatus(204);

			}
		})
	})
})

// Updates a product's description
app.patch('/product/:id_product', function(req, res){
	console.log(req.params)
	console.log(req.body);

	var post = [[
	req.params.id_product,
	req.body.product_desc
	]];

	verifyToken(req, res, [2,4], function(req, res, id) {
		if (!id) return;

		con.query('CALL uptdateProductDesc ?', [post], function(error, rows, fields){
			if (error){
				console.log(error);
				if (error.errno==1366)
					res.sendStatus(400);
				else res.sendStatus(404);

			} else {
				res.sendStatus(204);

			}
		})
	})
})


// Updates the product's price
app.patch('/product/:id_product/price/:product_price', function(req, res){
	console.log(req.params)
	console.log(req.body);

	var post = [[
	req.params.id_product,
	req.params.product_price
	]];

	verifyToken(req, res, [2,4], function(req, res, id) {
		if (!id) return;

		con.query('CALL uptdateProductPrice ?', [post], function(error, rows, fields){
			if (error){
				console.log(error);
				if (error.errno==1366)
					res.sendStatus(400);
				else res.sendStatus(404);

			} else {
				res.sendStatus(204);

			}
		})
	})
})


// Modifies a category
app.patch('/category/:id_category', function(req, res){
	console.log(req.params)
	console.log(req.body);

	verifyToken(req, res, [2,4], function(req, res, id) {
		if (!id) return;

		var post = [[
		req.params.id_category,
		req.body.name
		]];


		con.query('CALL uptdateCategory ?', [post], function(error, rows, fields){
			if (error){
				console.log(error);
				if (error.errno==1366)
					res.sendStatus(400);
				else res.sendStatus(404);

			} else {
				res.sendStatus(204);

			}
		})
	})
})


// Starts the server. It listens on port 8001.
app.listen(8001);
